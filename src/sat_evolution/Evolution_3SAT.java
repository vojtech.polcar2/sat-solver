package sat_evolution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author trapp
 */
public class Evolution_3SAT {
    
    private File file;
    BufferedReader br_file;
    private final List<Clause> clauses;
    private final List<Integer> weights;
    private List<Boolean> evaluated_best_set;
    private List<Boolean> bruteforce_best_set;
    private int clauses_count;
    private int variable_count;
    private int evaluated_best_cost;
    private int bruteforce_solution;
    private int population_limit;
    private final int elite_count;
    private int generations;
    private final double crossover_probability;
    private final double mutation_probability;
    private final int tournament_size;
    private List<Formula> population;
    private List<Formula> newPopulation;
    private long start_time;
    private long end_time;
    private long overall_time;
    private double worst_deviation;
    private double relative_deviation;
    private int cntOfSolved;
    private int cntOfProblems;
    private int cntOfSatisfied;
    
    Random rand;
    
    /* All evolution methods and file methods. */
    
    /*
        clauses - List of initial clauses that are in the formula.
        weights - List of weights of each variable.
        clauses_count - Number of clauses.
        variable_count - Number of variables.
        population_limit - First argument - limit of population in the evolution algorithm.
        generations - Second argument - number of generations of the evolution.
        elite_count - Third argument - number of elite Instances, which goes right to the next generation.
        crossover_probability - Third argument - decimal number of probability to crossover an Instance.
        mutation_probability - Fourth argument - decimal number with probability of inverting each item.
        tournament_size - Fifth argument - size of the tournament for choosing a parents.
        population - List of actual population.
        newPopulation - List of next population.
        rand - Random generator.
        start_time - Start time of the algorithm.
        end_time - end time of the algorithm.
        overall_time - overall computation time of all instances in one file.
        worst_deviation - Worst deviation of all instances in one file.
        relative_deviation - Relative deviation of all instances in one file.
    */
    
    Evolution_3SAT(String input_file, String [] args) throws FileNotFoundException {
        openFile(input_file);
        this.clauses = new ArrayList<>();
        this.weights = new ArrayList<>();
        this.evaluated_best_set = new ArrayList<>();
        this.bruteforce_best_set = new ArrayList<>();
        this.clauses_count = 0;
        this.variable_count = 0;
        this.evaluated_best_cost = 0;
        this.bruteforce_solution = -1;
        this.population_limit = Integer.parseInt(args[0]);
        this.generations = Integer.parseInt(args[1]);
        this.elite_count = Integer.parseInt(args[2]);
        this.crossover_probability = Double.parseDouble(args[3]);
        this.mutation_probability = Double.parseDouble(args[4]);
        this.tournament_size = Integer.parseInt(args[5]);
        this.population = new ArrayList<>();
        this.newPopulation = new ArrayList<>();
        this.rand = new Random(); 
        this.start_time = 0;
        this.end_time = 0;
        this.overall_time = 0;
        this.relative_deviation = 0;
        this.worst_deviation = 0;
        this.cntOfSolved = 0;
        this.cntOfProblems = 0;
        this.cntOfSatisfied = 0;
    }
    
    /*
        Method for opening the input file.
    */
    public void openFile(String input_file) throws FileNotFoundException{
        this.file = new File(input_file);
        this.br_file = new BufferedReader(new FileReader(this.file));
        System.out.println("Reading from a file: " + this.file.getName());
    }
    
    public void printClauses(){
        for( int i = 0; i < this.clauses.size(); i++){
            this.clauses.get(i).print();
            if( i < this.clauses_count - 1)
                System.out.print("*");
        }
        System.out.println();
    }
    
    /*
        Main solving function fo knapsack. It goes through all knapsack instances in the file and solving them.
    */
    public void solve() throws IOException {
        String input_line;
        double actual_deviation = 0;
        while ((input_line = br_file.readLine()) != null){
            if( input_line.length() == 0 )
                break;
            this.start_time = System.currentTimeMillis();
            clearResults();
            parseInput(input_line.split(" "));
            
            generateInitialPopulation();
            
            Collections.sort(population, Formula.FormulaComparator);
            
            //printClauses();
            
            if( this.variable_count <= 22 ){
                bruteForceSolution();
            }
            
            evolution();
            
            if( this.evaluated_best_cost == 0 ){
                doEvolutionAgain();
            }
            
            this.end_time = System.currentTimeMillis();
            this.overall_time += this.end_time - this.start_time;
            
            if( this.bruteforce_solution == 0 )
                this.relative_deviation += 0;
            else{
                actual_deviation = (((double)this.bruteforce_solution - (double)this.evaluated_best_cost) / (double)this.bruteforce_solution);
            
                this.relative_deviation += actual_deviation;
                if( actual_deviation > this.worst_deviation )
                    this.worst_deviation = actual_deviation;
            }
            
            getMeasureResults(actual_deviation);
        } 
        getMeasureValues();
    }
    
    private void doEvolutionAgain(){
        this.population.clear();
        this.newPopulation.clear();
        this.evaluated_best_set.clear();
        int tmpGeneration = this.generations;
        this.generations = this.generations * 2;
        int tmpPopulation = this.population_limit;
        this.population_limit = this.population_limit * 2;
        generateInitialPopulation();
        Collections.sort(population, Formula.FormulaComparator);

        evolution();       
        this.generations = tmpGeneration;
        this.population_limit = tmpPopulation;
    }

    /*
        Clearing results for another knapsack instance.    
    */
    private void clearResults() {
        this.clauses.clear();
        this.clauses_count = 0;
        this.variable_count = 0;
        this.weights.clear();
        this.population.clear();
        this.newPopulation.clear();
        this.evaluated_best_set.clear();
        this.evaluated_best_cost = 0;
        this.bruteforce_solution = -1;
        this.cntOfSatisfied = 0;
    }

    /*
        Parsing an input line to the variables.
    */
    private void parseInput(String [] input_array) throws IOException {
        String input_line;
        if( input_array[0].equals("c") ){
            while((input_line = br_file.readLine()) != null){
                input_line = input_line.trim().replaceAll("\\s+", " ");
                String [] array = input_line.split(" ");
                if(array[0].equals("c"))
                    continue;
                else{
                    this.variable_count = Integer.parseInt(array[2]); //number of variables
                    this.clauses_count = Integer.parseInt(array[3]); //number of clauses
                    break;
                }
            }
        } else{  
            this.variable_count = Integer.parseInt(input_array[2]); //number of variables
            this.clauses_count = Integer.parseInt(input_array[3]); //number of clauses
        }

        for(int i = 0; i < this.variable_count; i++ ){
            this.weights.add(rand.nextInt(9) + 1);
        }
        String [] actual_line;
        List<Literal> newLiterals = new ArrayList<>();
        while ((input_line = br_file.readLine()) != null){
            input_line = input_line.replaceAll("\\s+", " ");
            input_line = input_line.trim();
            actual_line = input_line.split(" ");
            
            if( actual_line[0].equals("%") )
                continue;
            
            // Last line.
            if( Integer.parseInt(actual_line[0]) == 0 && actual_line.length == 1 ){
                break;
            }
            
            for (String actual_line1 : actual_line) {
                if (Integer.parseInt(actual_line1) == 0) {
                    Clause newClause = new Clause(newLiterals);
                    this.clauses.add(newClause);
                    newLiterals = new ArrayList<>();
                }
                if (Integer.parseInt(actual_line1) < 0) {
                    Literal newLiteral = new Literal(Integer.parseInt(actual_line1) * -1, true);
                    newLiterals.add(newLiteral);
                }
                if (Integer.parseInt(actual_line1) > 0) {
                    Literal newLiteral = new Literal(Integer.parseInt(actual_line1), false);
                    newLiterals.add(newLiteral);
                }   
            }
        }
    }
    
    /*
        Generating an initial population.
    */
    private void generateInitialPopulation() {
        for( int i = 0; i < this.population_limit; i++){
            List<Boolean> newValues = new ArrayList<>();
            for( int j = 0; j < this.variable_count; j++ ){
                int value = rand.nextInt(20);
                if( value == 0 )
                    newValues.add(true);
                else
                    newValues.add(false);
            }
            Formula newForm = new Formula(newValues);
            
            newForm.isSatisfying(this.clauses);
            newForm.getFitness(this.weights);
            
            this.population.add(newForm);
        }     
    }    

    /*
        Main evolution method - it goes through the # of generations and create a new populations.
    */
    private void evolution() {
        for(int i = 0; i < this.generations; i++ ){
            cloneElites();
            
            //printPopulationDetails(i);
            
            List<Formula> parents = chooseParents();
            
            createsCrossovers(parents);
            
            mutate();
            
            doEvolutionStep();
        }
    }
    
    /*
        Supporting method, which print all populations weight and best/avg weights through the population.    
    */
    private void printPopulationDetails(int pop_id){
        System.out.print("Generation: " + pop_id + " - ");
        for(int j = 0; j < this.population_limit - 1; j++ ){
            System.out.print("("+this.population.get(j).getTotalWeight() + "," + this.population.get(j).getCntOfSatisfied() + ") ");
        }
        System.out.println();

        double avg = 0;
        for( int j = 0; j < this.population_limit - 1; j++ ){
            avg += this.population.get(j).getTotalWeight() ;
        }
        avg = avg / this.population_limit;
        System.out.println("Best/avg weight: " +this.population.get(0).getTotalWeight()  + " " + avg);
    }
    
    /*
        Method which clone a number of elites based on the argument to the next generation.
    */
    private void cloneElites(){
        for(int i = 0; i < this.elite_count; i++ ){
            newPopulation.add(this.population.get(i).clone());
        }
    }
    
    /*
        Method which do evolution step to the new generation.
    */
    private void doEvolutionStep(){
        population.clear();
        for( Formula f : newPopulation )
            population.add(f.clone());        
        newPopulation.clear();
        recalculateResults();
        Collections.sort(population, Formula.FormulaComparator);
        getBestInstance();
    }
    
    /*
        Method which recalculate fitness of population.
    */
    private void recalculateResults(){
        for( int i = 0; i < this.population_limit - 1; i++ ){
            this.population.get(i).isSatisfying(clauses);
            this.population.get(i).getFitness(weights);
        }
    }

    /*
        Function which write best solution to the console.
    */
    private void getBestInstance() {
        if( evaluated_best_cost < this.population.get(0).getTotalWeight() && this.population.get(0).getValue() ){
            this.evaluated_best_cost = this.population.get(0).getTotalWeight();
            this.evaluated_best_set.clear();
            for( int i = 0; i < this.variable_count; i++ )
                this.evaluated_best_set.add(this.population.get(0).values.get(i));
        }
    }

    /*
        Choosing a parents to the next generation with tournament function.
    */
    private List<Formula> chooseParents() {
        List<Formula> parents = new ArrayList<>();
        Formula tournamentWinner;
        for( int i = 0; i < this.population_limit - this.elite_count; i++ ){
            tournamentWinner = tournament();
            parents.add(tournamentWinner);
        }
        
        return parents;
    }
    
    /*
        Tournament function which select the best of the # of instances.
    */
    private Formula tournament(){
        List<Formula> tournamentInstances = new ArrayList<>();
        int value;
        int count = 0;
        while(count != this.tournament_size){
            value = rand.nextInt(((this.population_limit / 5) * 3) - this.elite_count) + 1;
            tournamentInstances.add(this.population.get(value));
            count++;
        }
        
        Collections.sort(tournamentInstances, Formula.FormulaComparator);
        
        return tournamentInstances.get(0);        
    }

    /*
        Function which pair crossovers and call a cross function.
    */
    private void createsCrossovers(List<Formula> parents) {
        double value;
        for( int i = 0; i < this.population_limit - 1 - elite_count; i = i + 2 ){
            value = rand.nextDouble();
            if( value <= this.crossover_probability ) {
                cross(parents.get(i), parents.get(i+1));
            }
            else{
                newPopulation.add(parents.get(i).clone());
                newPopulation.add(parents.get(i+1).clone());
            }
        }
        
        newPopulation.add(parents.get(this.population_limit - 1 - elite_count).clone());
     
    }

    /*
        Crossing function, which combinate two instances together and create a new one.
    */
    private void cross(Formula first, Formula second) {
        List<Boolean> crossedValues = new ArrayList<>();
        List<Boolean> crossedValuesSecond = new ArrayList<>();
        int value = rand.nextInt(this.variable_count - 1);
        for( int i = 0; i < value; i++ ){
            crossedValues.add(first.getValues().get(i));
            crossedValuesSecond.add(second.getValues().get(i));
        }
        
        for( int i = value; i < this.variable_count; i++ ){
            crossedValues.add(second.getValues().get(i));
            crossedValuesSecond.add(first.getValues().get(i));
        }
        
        Formula crossedFormula = new Formula(crossedValues);
        Formula crossedFormulaSecond = new Formula(crossedValuesSecond);
        
        crossedFormula.isSatisfying(clauses);
        crossedFormula.getFitness(weights);

        this.newPopulation.add(crossedFormula);
        if( this.newPopulation.size() != this.population_limit ){
            crossedFormulaSecond.isSatisfying(clauses);
            crossedFormulaSecond.getFitness(weights);

            this.newPopulation.add(crossedFormulaSecond);
        }
    }

    /*
        Mutation over all instances in new generation and all of their items.
    */
    private void mutate() {
        double value;
        
        for( int i = this.elite_count; i < this.population_limit - 1; i++ ){
            for( int j = 0; j < this.variable_count; j++ ){
                value = rand.nextDouble();
                if( value <= this.mutation_probability ){
                    if( this.newPopulation.get(i).values.get(j) )
                        this.newPopulation.get(i).values.set(j, false);
                    else
                        this.newPopulation.get(i).values.set(j, true);
                }
            }      
            this.newPopulation.get(i).isSatisfying(clauses);
            this.newPopulation.get(i).getFitness(weights);
        }
    }

    private void getMeasureResults(double actual_deviation) {
        System.out.println("Best weight is (EA/BF): " + this.evaluated_best_cost + "/" + this.bruteforce_solution);
        /*System.out.println("Values weights are: ");
        for( int i = 0; i < this.weights.size(); i++ )
            System.out.print(weights.get(i) + " ");
        System.out.println("");*/
        if( evaluated_best_cost > 0){
            System.out.println("Best set of values is: " + this.population.get(0).getValue());
            for(int i = 0; i < this.variable_count; i++ ){
                int val = this.evaluated_best_set.get(i) ? 1 : 0;
                System.out.print(val);
                if( i < this.variable_count - 1 )
                    System.out.print(" ");
            }
            System.out.println("");
        }
        else{
            System.out.println("Maximum of satisfied clauses is " + this.population.get(0).getCntOfSatisfied() + " with weight " + this.population.get(0).getTotalWeight() );
            for(int i = 0; i < this.variable_count; i++ ){
                int val = this.population.get(0).getValues().get(i) ? 1 : 0;
                System.out.print(val);
                if( i < this.variable_count - 1 )
                    System.out.print(" ");
            }
            System.out.println("");
        }
        if( (this.evaluated_best_cost != this.bruteforce_solution ) && this.bruteforce_solution != -1 ){
            System.out.println("Best set of values by BF is: ");
            for(int i = 0; i < this.variable_count; i++ ){
                int val = this.bruteforce_best_set.get(i) ? 1 : 0;
                System.out.print(val);
                if( i < this.variable_count - 1 )
                    System.out.print(" ");
            }
            System.out.println("");
        }
        if( this.population.get(0).getValue() )
            this.cntOfSolved++;
        this.cntOfProblems++;
        this.cntOfSatisfied = this.population.get(0).getCntOfSatisfied();
        System.out.println("EA_SAT time: " + ((double)this.end_time - (double)this.start_time));
        System.out.println("Actual deviation: " + actual_deviation);        
    }
    
    private void getMeasureValues(){
        System.out.println("Solved problems/# problems: " + this.cntOfSolved + "/" + this.cntOfProblems);
        System.out.println("EA_SAT time: " + ((double)this.overall_time / 50));
        System.out.println("Relative deviation: " + (this.relative_deviation / 50));
        System.out.println("Worst deviation: " + this.worst_deviation);
    }
    
    public int getCntOfSolved(){
        return cntOfSolved;
    }
    
    public int getCntOfProblems(){
        return cntOfProblems;
    }
    
    public long getOverallTime(){
        return overall_time;
    }
    
    public double getRelativeDeviation(){
        return relative_deviation;
    }
    
    public double getWorstDeviation(){
        return worst_deviation;
    }
    
    public int getMaximumSatisfied(){
        return cntOfSatisfied;
    }
    
    private void bruteForceSolution(){
        int best_weight = 0;
        List<Boolean> best_values = new ArrayList<>();;
        for(long i = 0; i < (1<<this.variable_count); i++){
            List<Boolean> newValues = new ArrayList<>();
            for(int j = 0; j < this.variable_count; j++){
                if( (i & ( 1 << j )) > 0){
                    newValues.add(true);
                }
                else
                    newValues.add(false);

            }
            Formula newForm = new Formula(newValues);
            
            newForm.isSatisfying(this.clauses);
            newForm.getFitness(this.weights);
            
            if( (best_weight < newForm.getTotalWeight() && newForm.getValue()) ){
                best_weight = newForm.getTotalWeight();
                best_values.clear();
                for(int j = 0; j < newValues.size(); j++ )
                    best_values.add(newValues.get(j));
            }
        }
        
        for(int i = 0; i < best_values.size(); i++ )
            this.bruteforce_best_set.add(best_values.get(i));
        
        this.bruteforce_solution = best_weight;
    }
}