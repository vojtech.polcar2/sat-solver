/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sat_evolution;

/**
 *
 * @author trapp
 */
public class Literal {
        
    private int variable;
    private boolean negation;
    
    public Literal(int variable, boolean negation){
        this.variable = variable;
        this.negation = negation;
    }
    
    public Literal clone(){
        Literal l = new Literal(this.variable, this.negation);
        return l;
    }
    
    public int getVariable(){
        return variable;
    }
    
    public boolean getNegation(){
        return negation;
    }    
    
    public void print(){
        if( negation )
            System.out.print("(-X"+variable+")");
        else
            System.out.print("X"+variable);                    
    }
}
